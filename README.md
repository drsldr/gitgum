# GitGum

**A collection of [Gum](https://github.com/charmbracelet/gum)-infused helper scripts for
Git**

The point is to automate some otherwise tedious tasks, like writing [Conventional
Commits](https://www.conventionalcommits.org/en/v1.0.0/) messages and finding the right
commit to rebase against.

## Dependencies

These scripts are written in Python, so you obviously need Python (suggested at least
version 3.11), as well as Git, and Gum. Beyond that, not much.

## License
These scripts are under the MIT License.

## Actual purpose
I use these scripts myself, and want to pack them to be installable via
[Nix](https://nixos.org/). The simplest way to do that seems to be to host them
somewhere, then let Nix sort out the rest. I _may_ end up including the nix-file in this
repository, but keep in mind that gets weirdly recursive, since it will be pulling the
same repository it came from... I dunno. I'll figure it out.
