{ pkgs }:

let wrappers = pkgs.lib.makeBinPath (with pkgs; [ python3 gum ]);
in

with pkgs; stdenv.mkDerivation rec {
  pname = "gitgum";
  version = "0.2.1";

  src = ./.;

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin
    cp -r gitgum $out/bin/
    cp gitgum_commit.py $out/bin/igc
    cp gitgum_rebase.py $out/bin/igrb
    runHook postInstall
  '';

  postFixup = ''
    # Wrap the scripts
    wrapProgram $out/bin/igc \
      --prefix PATH : "${wrappers}"
    wrapProgram $out/bin/igrb \
      --prefix PATH : "${wrappers}"
  '';
}
