{
  description = "GitGum utilities";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
        in
        {
          devShells.default = with pkgs;
            mkShell {
              name = "gitgumsh";
              buildInputs = with pkgs; [ python3 gum ];
            };

          packages.default = import ./default.nix {
            inherit pkgs;
          };
        }
      ) // {
      overlays.default = final: prev: {
        gitgum = import ./default.nix { pkgs = final; };
      };
    };
}
