#!/usr/bin/env python

# Copyright 2024 Jonas A. Hultén

"""GitGum Common utilities"""

import argparse
import os
import sys
from typing import List, Optional, Union
import textwrap
import subprocess

__SETTINGS = {"debug": False}

__GUM_ENV = {
    "GUM_CONFIRM_PROMPT_BORDER": "normal",
    "GUM_CONFIRM_PROMPT_PADDING": "0 1",
    "GUM_CONFIRM_PROMPT_BORDER_FOREGROUND": "3",
    "GUM_CONFIRM_SELECTED_FOREGROUND": "0",
    "GUM_CONFIRM_SELECTED_BACKGROUND": "12",
    "GUM_FILTER_INDICATOR": ">",
    "GUM_FILTER_INDICATOR_BOLD": "1",
    "GUM_FILTER_INDICATOR_FOREGROUND": "2",
    "GUM_FILTER_MATCH_BOLD": "1",
    "GUM_FILTER_MATCH_FOREGROUND": "2",
    "GUM_INPUT_CURSOR_FOREGROUND": "15",
    "GUM_INPUT_PROMPT_FOREGROUND": "2",
    "GUM_WRITE_CURSOR_FOREGROUND": "15",
}

os.environ.update(__GUM_ENV)


### Parser handling
def add_debug_flag(p: argparse.ArgumentParser) -> None:
    """Adds the bog-standard debug flag to the parser"""
    p.add_argument("-d", "--debug", help="Enable debug logging", action="store_true")


def parse_and_handle_debug(p: argparse.ArgumentParser) -> argparse.Namespace:
    """Parse the args from the parser and set debugging"""
    args = p.parse_args()
    if args.debug:
        set_debug(True)
        debug("Debugging enabled")
    return args


### Debug handling
def set_debug(val: bool) -> None:
    """Set the library debug setting"""
    __SETTINGS["debug"] = val


def debug(txt: str) -> None:
    """Debug printer"""
    if __SETTINGS["debug"]:
        print(f"DEBUG: {txt}")


### Error "handling"
def bail(txt: str) -> None:
    """Print error message and bail out"""
    print(f"ERROR! {txt}")
    sys.exit(1)


### Gum sanity check
def gum_on_path() -> None:
    """
    Check PATH to verify that gum is available

    Also store the path for use in `gum_call`
    """
    __SETTINGS["gum"] = call(["which", "gum"], err_msg="'gum' is not found on PATH!")


### Git sanity check
def inside_git_repo() -> None:
    """Check that the current working directory is inside a git repository"""
    call(
        ["git", "rev-parse", "--is-inside-work-tree"],
        err_msg="Current directory is not a git repository!",
    )


### Subprocess handling
def call(
    cmd: Union[List[str], str],
    err_msg: Optional[str] = None,
    non_zero_ok: bool = False,
    in_data: Optional[str] = None,
    interactive: bool = False,
) -> Optional[Union[str, int]]:
    """Call the command defined in `cmd`, error out with `err_msg` unless `non_zero_ok`
    is True. Returns the `stdout` if all is okay or the return code, if `non_zero_ok` is
    True."""
    err_msg = f"Invoking {(' '.join(cmd))!r} failed!" if err_msg is None else err_msg
    try:
        cp = subprocess.run(
            cmd,
            stdout=None if interactive else subprocess.PIPE,
            check=not non_zero_ok,
            input=in_data,
            encoding="utf8",
            shell=interactive,
        )
    except subprocess.CalledProcessError:
        bail(err_msg)

    r_val = cp.returncode
    if not (non_zero_ok or interactive):
        r_val = cp.stdout.strip()
    debug(
        f"{(' '.join(cmd) if isinstance(cmd, list) else cmd)!r} "
        + f"=> {textwrap.shorten(str(r_val), width=72)!r}"
    )
    return r_val


def gum_call(cmd: List[str], **kwargs) -> Optional[Union[str, int]]:
    """Auto-preface command with `gum`"""
    gum_path = __SETTINGS.get("gum", "gum")
    cmd = [gum_path] + cmd
    return call(cmd, **kwargs)
