#!/usr/bin/env python

# Copyright 2024 Jonas A. Hultén

"""GitGum Commit script"""

import argparse
from typing import List, Optional
from dataclasses import dataclass
import os.path as op
import textwrap
import re

from gitgum import common

### Globals
__SETTINGS = {
    "line_maxlen": 72,
    "types": ["feat", "fix", "chore", "docs", "style", "refactor", "ci", "test"],
}

SCOPE_PATTERN = re.compile(r"^\w+\((\w+)\)")


### Commit message class
@dataclass
class Commit:
    """Dataclass containing commit message components"""

    type: str = ""
    scope: str = ""
    summary: str = ""
    body: str = ""
    breaking: bool = False
    break_foot: str = ""

    def frontmatter(self) -> str:
        """Return the commit frontmatter (type and scope)"""
        return self.type + (f"({self.scope})" if len(self.scope) > 0 else "")

    def header(self) -> str:
        """Return the full header (frontmatter and summary)"""
        return self.frontmatter() + ("!" if self.breaking else "") + ": " + self.summary

    def argv(self, width: int) -> List[str]:
        """Return the git command array to perform the commit"""
        argv = ["git", "commit", f"--message={self.header()}"]
        if len(self.body) > 0:
            body = "\n".join(
                [textwrap.fill(l, width=width) for l in self.body.split("\n")]
            )
            argv.append(f"--message={body}")
        if len(self.break_foot) > 0:
            foot = textwrap.fill(f"BREAKING-CHANGE: {self.break_foot}", width=width)
            argv.append(f"--message={foot}")
        return argv


### Cache paths class
@dataclass
class CachePaths:
    """Simple dataclass for scope cache paths"""

    cache: str
    head_cache: str
    head: str


### Commit style class
@dataclass
class CommitStyle:
    """
    Bool wrapper for commit style

    In reality, this should probably be a Named Tuple, but there's no guarantee that
    both values are known at the same time.
    """

    long: Optional[bool]
    breaking: Optional[bool]


### Stage check
def check_staged() -> None:
    """Check that there's something staged for commit"""
    rc = common.call(["git", "diff", "--quiet", "--cached"], non_zero_ok=True)
    if rc == 0:
        common.bail("There's nothing staged for commit!")


### Sanity checks
def sanity() -> None:
    """Initial sanity checks"""
    # Check that we have Gum
    common.gum_on_path()

    # Check that we're in a git repo
    common.inside_git_repo()

    # Check that there's something staged
    check_staged()


# Scope cache handler
def read_scope_cache() -> List[str]:
    """Read (or create) the scope cache and return the list"""
    paths = scope_cache_paths()

    # Initialize a cache
    cache = set()

    # Check if a cache exists. If it does not, we'll need to create it and the
    # head-cache. If it does, we'll need to check the head cache to evaluate if we need
    # to re-cache.
    if op.isfile(paths.cache):
        # A cache exists. Open and read it, then proceed to check the head-cache
        with open(paths.cache, "rt", encoding="utf8") as f:
            for line in f:
                cache.add(line.strip())
        common.debug(f"Read {len(cache)} scopes from cache: {cache!r}")

        if op.isfile(paths.head_cache):
            # Next, compare contents of HEAD and its cache
            with open(paths.head, "rt", encoding="utf8") as head_f:
                git_head = head_f.read().strip()
            with open(paths.head_cache, "rt", encoding="utf8") as cache_f:
                cache_head = cache_f.read().strip()
            if git_head == cache_head:
                # Match. Return the cache
                cache = list(cache)
                cache.sort()
                common.debug(f"Returning cache: {cache!r}")
                return cache

    # Something above failed - regenerate the cache
    common.debug("Regenerating the scope cache, please stand by")

    # Turn the cache into a set
    cache = set(cache)

    # Get the log
    log = common.call(["git", "log", "--all", "--pretty=format:%s", "--max-count=1000"])

    # Scan the log for scopes
    for line in log.split("\n"):
        m = SCOPE_PATTERN.match(line)
        if m is not None:
            cache.add(m.group(1))

    # Back-convert the cache and write out
    cache = list(cache)
    cache.sort()
    with open(paths.cache, "wt", encoding="utf8") as f:
        f.write("\n".join(cache))

    # Replace the head-cache
    common.call(["cp", "-f", paths.head, paths.head_cache])

    common.debug(f"Got updated cache: {cache!r}")
    return cache


def scope_cache_paths() -> CachePaths:
    """Compile the paths used for the scope cache"""
    # Get path of the .git directory
    git_path = common.call(["git", "rev-parse", "--absolute-git-dir"])

    # Assemble the paths
    cache_path = op.join(git_path, ".gg-scope-cache")
    head_path = op.join(git_path, "HEAD")
    head_cache_path = op.join(git_path, ".gg-head")

    # Create the dataclass
    paths = CachePaths(cache=cache_path, head_cache=head_cache_path, head=head_path)
    return paths


def get_last_scope() -> str:
    """Gets the scope of the last commit"""
    log = common.call(["git", "log", "--pretty=format:%s", "--max-count=1"])
    m = SCOPE_PATTERN.match(log)
    if m is not None:
        scope = m.group(1)
        common.debug(f"Got latest scope: {scope!r}")
        return scope
    common.debug("Last commit had no scope")
    return ""


def update_cache(c: Commit, cache: List[str]) -> None:
    """Checks if the commit uses a new scope and adds it to the cache if that's the
    case"""
    if len(c.scope) > 0 and c.scope not in cache:
        common.debug(f"New scope {c.scope!r} will be added to cache")
        cache.append(c.scope)
        cache.sort()
        paths = scope_cache_paths()
        with open(paths.cache, "wt", encoding="utf8") as f:
            f.write("\n".join(cache))


### Gum wrapper section
def commit_type(c: Commit) -> None:
    """Prompt the user for a commit type"""
    c_type = common.gum_call(
        ["filter", "--header=Commit type"],
        err_msg="Failed to get type!",
        in_data="\n".join(__SETTINGS["types"]),
    )
    common.debug(f"Type: {c_type!r}")
    if c_type == "":
        common.bail("Invalid commit type!")
    c.type = c_type


def commit_scope(c: Commit) -> List[str]:
    """
    Prompt the user for a commit scope

    Returns the cache for later insertion checks
    """
    null_value = "[no scope]"
    cache = read_scope_cache()
    cache.append(null_value)
    last_scope = get_last_scope()
    scope = common.gum_call(
        [
            "filter",
            "--no-strict",
            "--header=Scope (optional)",
            f"--value={last_scope}",
        ],
        err_msg="Failed to get scope!",
        in_data="\n".join(cache),
    )
    common.debug(f"Scope: {scope!r}")
    if scope == null_value:
        return cache
    if not re.match(r"^[a-z0-9-]+$", scope):
        common.bail(f"Illegal character in scope {scope!r}")
    c.scope = scope
    return cache


def commit_summary(c: Commit) -> None:
    """Prompt the user for a commit summary"""
    prompt = c.frontmatter() + ": "
    max_len = (
        __SETTINGS["line_maxlen"]
        - len(prompt)
        - 1  # Space for "!" in case of breaking change
    )
    summary = common.gum_call(
        [
            "input",
            f"--header=Commit summary (max {max_len} characters)...",
            f"--char-limit={max_len}",
            f"--width={__SETTINGS['line_maxlen']}",
            f"--prompt={prompt}",
        ],
        err_msg="Failed to get summary!",
    )
    if len(summary) == 0:
        common.bail(f"No summary provided! (frontmatter: {c.frontmatter()})")
    common.debug(f"Summary: {summary!r}")
    c.summary = summary


def commit_style(
    c: Commit, long: Optional[bool], breaking: Optional[bool]
) -> CommitStyle:
    """
    Prompt the user for the style of commit

    Returns a CommitStyle object containing two bools detailing if the commit message
    should be long and/or contain a breaking changes footer.
    """
    cs = CommitStyle(long, breaking)
    # Short circuit return if all values are set
    if cs.long is not None and cs.breaking is not None:
        common.debug(f"Short-circuit style: {cs!r}")
        return cs

    # Compose the list of options
    styles = ["short", "short-breaking", "long", "long-breaking"]
    if cs.long is not None and cs.breaking is None:
        styles = ["non-breaking", "breaking"]
    if cs.breaking is not None and cs.long is None:
        styles = ["short", "long"]

    # Prompt
    style_str = common.gum_call(
        ["filter", f"--header=Style for '{c.header()}'"],
        err_msg="Failed to get style!",
        in_data="\n".join(styles),
    )
    common.debug(f"Style string: {style_str!r}")
    if style_str == "":
        common.bail("Invalid style!")

    # Process
    if cs.long is None:
        cs.long = "long" in style_str
        if cs.breaking is None:
            cs.breaking = "breaking" in style_str
    else:
        cs.breaking = "non" not in style_str

    common.debug(f"Commit style: {cs!r}")
    return cs


def commit_body(c: Commit) -> None:
    """Prompt the user for a commit body"""
    body = common.gum_call(
        [
            "write",
            f"--width={__SETTINGS['line_maxlen']}",
            "--char-limit=0",
            "--show-line-numbers",
            f"--header={c.header()}\nCommit body (CTRL+D to finish)",
        ],
        err_msg="Failed to get commit body!",
    )
    common.debug(f"Body: {body!r}")
    c.body = body


def commit_break(c: Commit) -> None:
    """Prompt the user for a breaking change footer"""
    footer = common.gum_call(
        [
            "write",
            f"--width={__SETTINGS['line_maxlen']}",
            "--char-limit=0",
            "--show-line-numbers",
            "--cursor.foreground=1",
            f"--header={c.header()}\nBREAKING-CHANGE footer (CTRL+D to finish)",
        ],
        err_msg="Failed to get breaking change footer!",
    )
    common.debug(f"BREAKING-CHANGE: {footer!r}")
    c.break_foot = footer


### Parser
PARSER = argparse.ArgumentParser(description="Gum-infused git commit helper")
PARSER.add_argument(
    "--long",
    help="Long commit (summary and body)",
    action=argparse.BooleanOptionalAction,
)
PARSER.add_argument(
    "--breaking",
    help="Breaking change in commit",
    action=argparse.BooleanOptionalAction,
)
common.add_debug_flag(PARSER)


### Entrypoint
def main() -> None:
    """Script entrypoint"""
    args = common.parse_and_handle_debug(PARSER)

    # Move to sanity checks
    sanity()

    # Script proper
    c = Commit()
    commit_type(c)
    cache = commit_scope(c)
    commit_summary(c)
    style = commit_style(c, args.long, args.breaking)
    if style.long:
        commit_body(c)
    if style.breaking:
        c.breaking = True
        commit_break(c)

    update_cache(c, cache)
    common.debug(c)

    git_cmd = c.argv(__SETTINGS["line_maxlen"])
    commit = common.call(git_cmd)
    print(commit)


if __name__ == "__main__":
    main()
