#!/usr/bin/env python

# Copyright 2024 Jonas A. Hultén

"""GitGum Rebase script"""

import argparse

from gitgum import common


### Sanity checks
def sanity() -> None:
    """Initial sanity checks"""
    # Check that we have Gum
    common.gum_on_path()

    # Check that we're in a git repo
    common.inside_git_repo()


### Rebase helper
def rebase_hash() -> str:
    """Prompt the user for which commit to rebase against"""
    log = common.call(
        ["git", "log", "--pretty=%h -%d %s (%ar) <%an>"],
        err_msg="Could not get git log",
    )

    log_line = common.gum_call(
        ["filter", "--no-fuzzy", "--header=Commit to rebase on"],
        err_msg="No commit selected",
        in_data=log,
    )

    if log_line == "":
        common.bail("Invalid commit selected!")

    commit_hash = log_line.split(" ")[0]
    common.debug(f"Hash: {commit_hash!r}")
    return commit_hash


### Parser
PARSER = argparse.ArgumentParser(description="Gum-infused git rebase helper")
common.add_debug_flag(PARSER)


### Entrypoint
def main() -> None:
    """Script entrypoint"""
    common.parse_and_handle_debug(PARSER)

    git_hash = rebase_hash()
    common.call(f"git rebase --interactive {git_hash}~", interactive=True)


if __name__ == "__main__":
    main()
